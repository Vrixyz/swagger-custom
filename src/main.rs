#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
extern crate rocket_contrib;
extern crate serde_json;
#[macro_use] extern crate serde_derive;
extern crate dotenv;

use dotenv::dotenv;
use std::env;
use rocket_contrib::templates::Template;
use rocket_contrib::serve::StaticFiles;
use rocket::response::Redirect;

#[derive(Serialize)]
struct TemplateSwaggerContext {
    api_server_doc_url: String,
}
#[derive(Serialize)]
struct TemplateDocContext {
    api_server_base_url: String,
}

#[get("/swagger")]
fn swagger() -> Template {
    let context = TemplateSwaggerContext { api_server_doc_url: env::var("API_SERVER_DOC_URL").unwrap() };
    Template::render("swagger/index", &context)
}
#[get("/doc")]
fn doc(addr: std::net::SocketAddr) -> Template {
    println!("{:#?}", addr);
    let context = TemplateDocContext { api_server_base_url: env::var("API_SERVER_BASE_URL").unwrap() };
    Template::render("doc_openapi", &context)
}
#[get("/")]
fn redirect(addr: std::net::SocketAddr) -> Redirect {
    Redirect::permanent("/swagger")
}

fn main() {
    dotenv().ok();
    rocket::ignite()
        .mount("/", routes![redirect, swagger, doc])
        .mount("/", StaticFiles::from("doc"))
        .attach(Template::fairing())
        .launch();
}